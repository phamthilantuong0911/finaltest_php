<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title><?php defineblock('title') ?></title>

    <!-- Bootstrap core CSS -->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo asset('assets/web/assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo asset('assets/web/assets/css/responsive.css') ?>">
    <link rel="stylesheet" href="<?php echo asset('assets/web/assets/css/plugins.css') ?>">
    <link rel="stylesheet" href="<?php echo asset('assets/web/assets/css/style.css') ?>">

</head>

<body>

    <!-- ***** Preloader Start ***** -->
    <?php include('views/web/layouts/includes/header.php') ?>

    <!-- Page Content -->
    <?php defineblock('content') ?>


    <?php include('views/web/layouts/includes/footer.php') ?>



    <!-- Bootstrap core JavaScript -->


    <!-- Additional Scripts -->
    <script src="<?php echo asset('assets\web\assets\js\vendor\instagram-feed.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\vendor\jquery-3.3.1.min.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\vendor\jquery.cookie.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\vendor\masonry.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\vendor\modernizr-3.6.0.min.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\vendor\photoswipe-ui-default.min.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\vendor\photoswipe.min.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\vendor\wow.min.js') ?>"></script>
   
    <script src="<?php echo asset('assets\web\assets\js\bootstrap.min.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\lazysizes.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\main.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\plugins.js') ?>"></script>
    <script src="<?php echo asset('assets\web\assets\js\popper.min.js') ?>"></script>


</body>

</html>

<?php require_once('views/web/layouts/index.php') ?>

<?php startblock('content') ?>

<div class="latest-products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Đăng kí</h2>
                </div>
            </div>
            <div class="col-md-12">
                <form class="form" action="<?php echo url('auth/handleRegister') ?>" method="POST">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control <?php echo !empty($errors['email']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['email'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['email'] ?></div>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" name="password" class="form-control <?php echo !empty($errors['password']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['password'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['password'] ?></div>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Xác nhận mật khẩu</label>
                        <input type="password" name="password_confirmation" class="form-control <?php echo !empty($errors['password_confirmation']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['password_confirmation'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['password_confirmation'] ?></div>
                        <?php } ?>
                    </div>

                    <button type="submit" class="btn btn-primary">Đăng kí</button>
                </form>
                <br/>

                <p>Đã có tài khoản, ấn vào đây để <a href="<?php echo url('auth/login') ?>">đăng nhập</a></p>

            </div>
        </div>
    </div>
</div>

<?php endblock() ?>
<?php require_once('views/web/layouts/index.php') ?>
<?php require_once('core/Flash.php'); ?>

<?php startblock('title') ?>
Auth
<?php endblock() ?>

<?php startblock('content') ?>

<div class="latest-products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Đăng nhập</h2>
                </div>
            </div>
            <div class="col-md-12">
                <?php if (Flash::has('error')) { ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?php echo Flash::get('error') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <form class="form" action="<?php echo url('auth/handleLogin') ?>" method="POST">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control <?php echo !empty($errors['email']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['email'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['email'] ?></div>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control <?php echo !empty($errors['password']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['password'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['password'] ?></div>
                        <?php } ?>
                    </div>

                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
                <br/>
                <p>Chưa có tài khoản, ấn vào đây để <a href="<?php echo url('auth/register') ?>">đăng ký</a></p>
            </div>
        </div>
    </div>
</div>

<?php endblock() ?>
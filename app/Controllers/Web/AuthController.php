<?php
require_once('app/Controllers/Web/WebController.php');
require_once('app/Models/User.php');
require_once('app/Requests/AuthRequest.php');
require_once('core/Flash.php');
require_once('core/Auth.php');

class AuthController extends WebController
{
    public function login()
    {
        return $this->view('auth/login.php');
    }

    public function register()
    {
        return $this->view('auth/register.php');
    }

    public function handleRegister()
    {
        $authRequest = new AuthRequest();
        $errors = $authRequest->validateRegister($_POST);

        if (count($errors) == 0) {
            $user = new User();
            $_POST['password'] = md5($_POST['password']);
            $createdUser = $user->create($_POST);
            if ($createdUser) {
                return redirect('auth/login');
            }
        }

        return $this->view('auth/register.php', ['errors' => $errors]);
    }

    public function handleLogin()
    {
        $authRequest = new AuthRequest();
        $errors = $authRequest->validateLogin($_POST);
        if (empty($errors)) {
            $user = new User();
            $foundUser = $user->first($_POST);
            if ($foundUser) {
                Auth::setUser('user', $foundUser);
                return 'Found';
            }
            Flash::set('error', 'Đăng nhập thất bại');
            return redirect('auth/login');
        }

        return $this->view('auth/login.php', ['errors' => $errors]);
    }

    public function logout()
    {
        Auth::logout('user');
        return redirect('auth/login');
    }
}

